﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Board.Repositories;

namespace Board.Controllers
{
    public class AdController : ApiController
    {
        private readonly AdRepository _adRepository;

        public AdController(AdRepository adRepository)
        {
            _adRepository = adRepository;
        }


        // GET: api/Ad
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Ad/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Ad
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Ad/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Ad/5
        public void Delete(int id)
        {
        }
    }
}
