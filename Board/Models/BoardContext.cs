using Board.Models.DataModels;
using System.Data.Entity;

namespace Board.Models
{

    public class BoardContext : DbContext
    {
        public BoardContext(): base("name=BoardContext"){}

        public virtual DbSet<Ad> Ads { get; set; }
        public virtual DbSet<File> Files { get; set; }
        public virtual DbSet<User> Users { get; set; }
    }
}