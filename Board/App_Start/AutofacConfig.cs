﻿using System.Data.Entity;
using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Board.Models;
using Board.Repositories;

namespace Board
{
    public class AutofacConfig
    {
        public static void Config()
        {
            var builder = new ContainerBuilder();

            var config = GlobalConfiguration.Configuration;
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiFilterProvider(config);


            builder.RegisterType<BoardContext>().As<DbContext>().InstancePerRequest();


            builder.RegisterType<AdRepository>();
            builder.RegisterType<UserRepository>();
            builder.RegisterType<FileRepository>();


            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}